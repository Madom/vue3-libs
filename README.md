# vue3-libs

### 安装

#### 用配套的cli插件
vue add vue-plus-libs


#### 手动安装

npm i vue3-libs
```typescript
import { createApp } from 'vue'
import App from './App.vue'
import AntUi from 'vue3-libs';

// 全部引入
createApp(App).use(AntUi, { message: { key: 'customKey', type: 'error' } }).mount('#app')
```

```typescript
import { createApp } from 'vue'
import App from './App.vue'
import { Tabs, Form, Field, Message } from 'vue3-libs';

// 部分引入
createApp(App)
  .use(Tabs)
  .use(Form)
  .use(Field)
  .use(Message, { key: 'customKey', type: 'error' })
  .mount('#app')
```
