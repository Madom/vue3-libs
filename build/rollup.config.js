// 处理第三方module
import { nodeResolve } from '@rollup/plugin-node-resolve'
import path from 'path'
// import commonjs from '@rollup/plugin-commonjs' // commonjs转es6, 前端代码不需要
import { terser } from 'rollup-plugin-terser'
import typescript from 'rollup-plugin-typescript2'
import css from 'rollup-plugin-css-only'
import scss from 'rollup-plugin-scss'
import babel from '@rollup/plugin-babel';
import pkg from '../package.json'
const deps = Object.keys(pkg.dependencies)
import vue from 'rollup-plugin-vue';
import del from 'rollup-plugin-delete';

const baseOutput = format => ({
  format,
  file: `lib/${format}/index.js`
});

const delDir = format => {
  const target = [`lib/${format}`];
  if (format === 'esm') {
    target.push('lib/types');
  }
  return target;
};

export default ({ format }) => {
  const isEsm = format === 'esm';
  const output = isEsm ? baseOutput(format) : {
    ...baseOutput(format),
    name: 'vue-next-libs-demo',
    exports: 'named',
    // async-validator和mitt也要声明，不过rollup的猜测是对的，所以可不写
    globals: { vue: 'Vue' }
  }
  return [
    {
      input: path.join(__dirname, '..', 'src/components/index.ts'),
      output,
      plugins: [
        del({ targets: delDir(format) }),
        terser(),
        nodeResolve(),
        scss({
          exclude: ['node_modules'],
          outputStyle: 'compressed'
        }),
        css(),
        vue({
          target: 'browser',
          css: false,
          exposeFilename: false
        }),
        typescript(isEsm ? { // 或者build type单独用tsc完成？
          useTsconfigDeclarationDir: true,
          tsconfig: 'src/components/tsconfig.json'
        } : {}),
        babel({
          exclude: 'node_modules/**',
          extensions: ['.js', '.jsx', '.ts', '.tsx', '.vue'],
          babelHelpers: 'runtime'
        })
      ],
      external(id) {
        // 这里排除的，global里都要声明
        return /^vue/.test(id) || deps.some(k => new RegExp('^' + k).test(id));
      },
    },
  ]
}
