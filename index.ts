export { default as Field } from './src/components/Field';
export { default as Form } from './src/components/Form';
export { default as Tabs } from './src/components/Tabs';
export { default as Message }from './src/components/Message';
import AntUi from './src/components';
import './src/assets/styles/index.scss';


export default AntUi;
