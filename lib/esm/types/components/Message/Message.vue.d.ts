import './index.scss';
import { MessageItemData } from "./index";
declare const _default: import("vue").DefineComponent<{}, {
    createMessage: (item: MessageItemData) => void;
    emitter: import("mitt").Emitter;
    handleRemove: (id: string) => void;
    instances: import("vue").Ref<{
        messageId: string;
        content: string;
        onClose: {
            all: import("mitt").EventHandlerMap;
            on: {
                <T = any>(type: string | symbol, handler: import("mitt").Handler<T>): void;
                (type: "*", handler: import("mitt").WildcardHandler): void;
            };
            off: {
                <T_1 = any>(type: string | symbol, handler: import("mitt").Handler<T_1>): void;
                (type: "*", handler: import("mitt").WildcardHandler): void;
            };
            emit: {
                <T_2 = any>(type: string | symbol, event?: T_2 | undefined): void;
                (type: "*", event?: any): void;
            };
        };
        show: boolean;
        options: {
            type: "info" | "success" | "warning" | "error";
            duration: number;
            pauseOnHover: boolean;
            animate: boolean;
        };
    }[]>;
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, import("vue").EmitsOptions, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<{} & {}>, {}>;
export default _default;
