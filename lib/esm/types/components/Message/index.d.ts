import { App } from 'vue';
import { Emitter } from 'mitt';
declare type theme = 'info' | 'success' | 'warning' | 'error';
export interface MessageOptions {
    type: theme;
    duration: number;
    pauseOnHover: boolean;
    animate: boolean;
}
export interface MessageItemData {
    messageId: string;
    content: string;
    onClose: Emitter;
    show: boolean;
    options: MessageOptions;
}
export declare class MessageService {
    private message;
    private overlay;
    private defOptions;
    readonly emitter: Emitter;
    constructor(options?: Partial<MessageOptions>);
    create(content: string, options?: Partial<MessageOptions>): {
        messageId: string;
        content: string;
        show: boolean;
        options: MessageOptions;
        onClose: Emitter;
    };
    private getMessage;
}
export interface MessagePluginOptions extends MessageOptions {
    key: string;
}
export default function (app: App, options?: MessagePluginOptions): void;
export {};
