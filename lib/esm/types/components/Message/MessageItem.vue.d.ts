import { PropType } from 'vue';
import { MessageItemData } from "./index";
declare const _default: import("vue").DefineComponent<{
    id: {
        type: StringConstructor;
        require: boolean;
    };
    message: {
        type: PropType<MessageItemData>;
        require: boolean;
    };
}, {
    handleAfterLeave: () => void;
    handleMouse: (type: 'enter' | 'leave') => void;
    handleRemove: () => void;
}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, ("close" | "remove")[], "close" | "remove", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<{} & {
    id?: string | undefined;
    message?: MessageItemData | undefined;
}>, {}>;
export default _default;
