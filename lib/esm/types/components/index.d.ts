import { App } from 'vue';
import Field from './Field';
import Form from './Form';
import FormItem from './FormItem';
import Tabs from './Tabs';
import TabPane from './TabPane';
import Message, { MessagePluginOptions } from "./Message";
interface AntOptions {
    message: MessagePluginOptions;
}
export { Field, Form, FormItem, Tabs, TabPane, Message };
export default function (app: App, options?: AntOptions): void;
