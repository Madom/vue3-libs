import { PropType } from "vue";
import { Rules } from "async-validator";
import './index.scss';
declare const _default: import("vue").DefineComponent<{
    model: ObjectConstructor;
    rules: PropType<Rules>;
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, ("submit" | "fail")[], "submit" | "fail", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<{} & {
    model?: Record<string, any> | undefined;
    rules?: Rules | undefined;
}>, {}>;
export default _default;
