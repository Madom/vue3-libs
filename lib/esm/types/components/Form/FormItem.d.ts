import { PropType } from "vue";
import { RuleItem } from 'async-validator';
declare const _default: import("vue").DefineComponent<{
    label: {
        type: StringConstructor;
        default: string;
    };
    prop: {
        type: StringConstructor;
        default: string;
    };
    errMsg: {
        type: StringConstructor;
        default: string;
    };
    rules: PropType<RuleItem | RuleItem[]>;
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, Record<string, any>, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<{
    label: string;
    prop: string;
    errMsg: string;
} & {
    rules?: RuleItem | RuleItem[] | (RuleItem & object) | (RuleItem[] & object) | undefined;
}>, {
    label: string;
    prop: string;
    errMsg: string;
}>;
export default _default;
