export declare const FormKey = "AntForm";
export declare const FormItemKey = "AntFormItem";
export interface FormItemContext {
    id: string;
    prop?: string;
    validate(): void;
    handlerValueChange?(value: any): void;
    handlerControlBlur?(value: any): void;
}
export interface FormContext {
    model: Record<string, any>;
    rules: Record<string, any>;
    addItem(child: FormItemContext): void;
    removeItem(id: string): void;
}
export declare type validFunc = (callback: (valid: boolean) => void) => Promise<any>;
