import { Emitter } from 'mitt';
interface OverlayConfig {
    bgColor: string;
    pointerEvents: string;
    animated: boolean;
}
export declare function useOverlay(userConfig?: Partial<OverlayConfig>): {
    emitter: Emitter;
    overlay: HTMLElement;
} | null;
export {};
