import { createApp } from 'vue'
import App from './App.vue'
import AntUi, { Message } from './components';
import './assets/styles/index.scss';

createApp(App).use(AntUi).use(Message,{ key: 'customKey' }).mount('#root')
