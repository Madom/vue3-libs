import { App } from "vue";
import TabPane from '../Tabs/TabPane';

TabPane.install = (app: App): void => {
  app.component(TabPane.name, TabPane)
}

export default TabPane;
