import { App } from 'vue';
import Field from './Field';
import Form from './Form';
import FormItem from './FormItem';
import Tabs from './Tabs';
import TabPane from './TabPane';
import Message, {MessagePluginOptions, MessageService} from "./Message";

const components = [
  Field,
  Form,
  FormItem,
  Tabs,
  TabPane
]

interface AntOptions {
  message: MessagePluginOptions;
}


export {
  Field,
  Form,
  FormItem,
  Tabs,
  TabPane,
  Message
}

export default function (app: App, options?: AntOptions) {
  components.forEach(item => app.component(item.name, item));
  const message = new MessageService(options?.message);
  app.provide<MessageService>(options?.message?.key || 'messageKey', message);
}
