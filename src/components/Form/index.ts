import { App } from "vue";
import Form from './Form';

Form.install = (app: App): void => {
  app.component(Form.name, Form)
}

export default Form;
