import { App } from "vue";
import Field from './Field';

// 既可以用于app.component，也可用于app.use
Field.install = (app: App): void => {
  app.component(Field.name, Field)
}

export default Field;
