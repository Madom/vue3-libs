import {defineComponent, inject, onBeforeUnmount, onMounted, ref, Transition} from "vue";
import {TabContext, TabsKey} from "./types";


export default defineComponent({
  name: 'TabPane',
  props: {
    name: {
      type: String,
      default: ''
    }
  },
  setup(props, { slots }) {
    const parent = inject<TabContext>(TabsKey);
    const show = ref(false);
    const changeVisible = (visible: boolean) => {
      show.value = visible;
    }
    onMounted(() => {
      parent?.addPanel({
        name: props.name,
        defSlot: slots.default!,
        titleSlot: slots.title!,
        changeVisible
      });
    });
    onBeforeUnmount(() => {
      parent?.removePanel(props.name);
    });
    return () => {
      return <div class="panes" v-show={ show.value }>{ slots.default!() }</div>
    }
  }
});
