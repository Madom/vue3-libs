import {Slot} from "vue";

export const TabsKey = 'AntTabs';

export interface TabPaneContext {
  name: string;
  defSlot: Slot;
  titleSlot: Slot;
  changeVisible: (visible: boolean) => void;
}

export interface TabContext {
  addPanel(item: TabPaneContext): void;
  removePanel(name: string): void;
}
