import { App } from "vue";
import Tabs from './Tabs';


Tabs.install = (app: App): void => {
  app.component(Tabs.name, Tabs)
}

export default Tabs;
