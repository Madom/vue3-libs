import { App } from "vue";
import FormItem from '../Form/FormItem';

FormItem.install = (app: App): void => {
  app.component(FormItem.name, FormItem)
}

export default FormItem;
